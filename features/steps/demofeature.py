from behave import *
from assertpy import assert_that

from helpers.weatherservice import WeatherService

use_step_matcher("re")


@step("we have a connection to a weather service")
def step_impl(context):
    weather_service = WeatherService()
    context.weather_service = weather_service

    status = weather_service.check_connection()
    assert_that(status).is_true()


@step("we request (.*) weather")
def step_impl(context, wanted_weather):
    weather_service : WeatherService = context.weather_service
    status = weather_service.set_wanted_weather(wanted_weather)
    assert_that(status).is_true()


@step("ask for a weather prediction")
def step_impl(context):
    weather_service : WeatherService = context.weather_service
    response = weather_service.get_weather()
    assert_that(response.status_code).is_equal_to(200)
    context.weather_response = response.json()


@step("we should receive nice weather")
def step_impl(context):
    weather_response : dict = context.weather_response

    assert_that(weather_response).is_length(5)

    temperatures = [w['temperatureC'] for w in weather_response]
    assert_that(all([t == 25 for t in temperatures])).is_true()
