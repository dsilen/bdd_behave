# Created by daniel at 2023-10-18
Feature: Weather settings
  # Enter feature description here

  Scenario: when requesting nice weather, nice weather should be presented
    Given we have a connection to a weather service
    When we request Nice weather
    And ask for a weather prediction
    Then we should receive nice weather