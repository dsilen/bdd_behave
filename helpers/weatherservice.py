import requests


class WeatherService:
    """Weather service client"""

    def __init__(self, baseurl = 'http://localhost:5084'):
        self.baseurl = baseurl

    def check_connection(self):
        response = requests.get(self.baseurl + '/swagger/index.html')
        return response.status_code == 200

    def set_wanted_weather(self, wanted_weather):
        response = requests.post(self.baseurl + '/setRequestedWeather', json={'weatherType': wanted_weather})
        return response.status_code == 200

    def get_weather(self):
        return requests.get(self.baseurl + '/weatherForecast')

